var express 		= require('express');
var app 			= express();
var port 			= process.env.PORT || 3000;
var mongoose 		= require('mongoose');
var passport 		= require('passport');				//https://github.com/jaredhanson/passport
var flash 			= require('connect-flash');			//https://github.com/jaredhanson/connect-flash
var jade 			= require('jade'); 					//https://www.npmjs.com/package/jade
var configDB 		= require('./config/database.js');
var compression 	= require('compression')
var bodyParser 		= require('body-parser');
var cookieParser 	= require('cookie-parser');
var session 		= require('express-session');
var shortid         = require('shortid');
var server 			= app.listen(port);
var io 				= require('socket.io').listen(server);


mongoose.connect(configDB.url);

require('./config/passport')(passport);

app.use(compression());
app.use(cookieParser());
app.use(bodyParser());
app.use(express.static(__dirname + '/app/static'));
app.set('view engine', 'jade');

app.use(session({ secret: 'asdasfasasdfsfgsdfgdf'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./app/routes.js')(app, passport);



/********************** MODELS *********************/

var userModel           = require('./app/models/user');



/****************************** SOCKET IO ********************************/


var twoPlayers = [];		// ARRAY OF SIZE 2 THAT STORED TWO PLAYERS IN A GAME
var userSockets = {};		// CONTAINS ALL SOCKETS OF USERS AND DATA STORED IN THE USER SOCKET
var gameRooms = {};			// CONTAINS THE IDS AND TWOPLAYERS ARRAYS OF ALL GAMES.


io.sockets.on('connection', function(socket) {


/* IF A USER PRESSES MATCHMAKING, INITIALIZE QUEUE AND STORE USER IN TWOPLAYERS ARRAY
 * ONCE ARRAY IS FILLED WITH 2 USERS
 * CREATE A GAME ID AND LET BOTH SOCKETS JOIN GAMEID ROOM
 * RESET TWOPLAYERS ARRAY
 ***********************************************************************************/

socket.on('unranked_queue', function(data) {

	if(!twoPlayers.contains(data.username)) {
		socket.username = data.username;
		socket.first = data.first;
		socket.second = data.second;
		socket.third = data.third;
		socket.forth = data.forth;
		socket.fifth = data.fifth;
		twoPlayers.push(socket.username);
		userSockets[data.username] = socket;
	}

	if(twoPlayers.length == 2) {

		var gameID = shortid.generate();
		gameRooms[gameID] = twoPlayers;

		for (var i = 0; i < twoPlayers.length; i++) {
			userSockets[twoPlayers[i]].join(gameID);
			userSockets[twoPlayers[i]].room = gameID;
			userSockets[twoPlayers[i]].ready = 'false';
		}

		io.sockets.in(gameID).emit('unranked_queue', {'players': twoPlayers, 'gameID': gameID});
		twoPlayers = [];
	}

});


/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* ON READY SET SOCKET DATA READY
 * ONCE BOTH PLAYER SOCKETS ARE READY SET TO TRUE
 * SEND MATCH BEGIN AND CARD POSITIONS
 **************************************/
socket.on('ready', function(data) {

	socket.first = initHero(data.first);
	socket.second = initHero(data.second);
	socket.third = initHero(data.third);
	socket.forth = initHero(data.forth);
	socket.fifth = initHero(data.fifth);
	socket.ready = 'true';
	
	gameRooms[socket.room].turnArray = [];
	var Arrayplayers = gameRooms[socket.room];
	var opponent = userSockets[getGameRoomOpponent(gameRooms, socket.username)];

	if(bothReady(Arrayplayers)) {

		var randTurn = Math.floor((Math.random() * 2) + 1);
		
		socket.ready = 'false';
		opponent.ready = 'false';

		// GIVE LAST READIED USER THE FIRST TURN
		if(randTurn == 2) {
			gameRooms[socket.room].turnArray.push(socket.username);
			gameRooms[socket.room].turnArray.push(opponent.username);
		}

		//GIVE FIRST READIED USER THE FIRST TURN
		else {
			gameRooms[socket.room].turnArray.push(opponent.username);
			gameRooms[socket.room].turnArray.push(socket.username);
		}

		console.log('turn list is: ' + gameRooms[socket.room].turnArray);

		// SEND TO THE LAST USER READIED
		socket.emit('match start', {'enemy_first': opponent.first,
			'enemy_second': opponent.second,
			'enemy_third': opponent.third,
			'enemy_forth': opponent.forth,
			'enemy_fifth': opponent.fifth,
			'goesfirst': gameRooms[socket.room].turnArray[0],
			'first': socket.first,
			'second': socket.second,
			'third': socket.third,
			'forth': socket.forth,
			'fifth': socket.fifth});

		// SEND TO THE FIRST USER READIED OR THE OTHER USER
		opponent.emit('match start', {'enemy_first': socket.first,
			'enemy_second': socket.second,
			'enemy_third': socket.third,
			'enemy_forth': socket.forth,
			'enemy_fifth': socket.fifth,
			'goesfirst': gameRooms[socket.room].turnArray[0],
			'first': opponent.first,
			'second': opponent.second,
			'third': opponent.third,
			'forth': opponent.forth,
			'fifth': opponent.fifth});
	}

});

/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* SET SOCKET READY TO FALSE
 **************************************/
socket.on('not ready', function() {
		userSockets[socket.username].ready = 'false';
});


/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* SOCKET EMOTES
*******************************************************/

socket.on('gl hf', function() {
	userSockets[getGameRoomOpponent(gameRooms, socket.username)].emit('gl hf');
});

socket.on('gg', function() {
	userSockets[getGameRoomOpponent(gameRooms, socket.username)].emit('gg');
});

socket.on('wow', function() {
	userSockets[getGameRoomOpponent(gameRooms, socket.username)].emit('wow');
});

socket.on('nice', function() {
	userSockets[getGameRoomOpponent(gameRooms, socket.username)].emit('nice');
});

socket.on('lol', function() {
	userSockets[getGameRoomOpponent(gameRooms, socket.username)].emit('lol');
});

socket.on('rip', function() {
	userSockets[getGameRoomOpponent(gameRooms, socket.username)].emit('rip');
});

socket.on('thanks', function() {
	userSockets[getGameRoomOpponent(gameRooms, socket.username)].emit('thanks');
});

socket.on('flip1', function() {
	userSockets[getGameRoomOpponent(gameRooms, socket.username)].emit('flip1');
});

socket.on('flip2', function() {
	userSockets[getGameRoomOpponent(gameRooms, socket.username)].emit('flip2');
});

/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* WHEN A USER QUITS SEARCHING MID QUEUE
 * REMOVE USER FROM TWOPLAYERS ARRAY
 ***************************************/
socket.on('quit unranked_queue', function() {
		twoPlayers.splice(twoPlayers.indexOf(socket.username), 1);
});



/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* USER IS DEFEATED, EITHER SURRENDER OR
 * JUST LOST THE MATCH
 ****************************************/
socket.on('defeat', function(data) {

	var winner = getGameRoomOpponent(gameRooms, socket.username);
	userSockets[winner].leave(socket.room);
	socket.leave(socket.room);
	userSockets[winner].emit('victory', {'player': winner});
	userSockets[socket.username].emit('defeat', {'player': data});

	updatePlayerStats(winner, socket.username);
	delete gameRooms[socket.room];
	console.log('game rooms after disconnect game end: ' + JSON.stringify(gameRooms));

});


/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* FIRST CHECK IF SOCKET IS IN A GAME
 * IF SO, GET THE OTHER USER IN ARRAY VALUE OF GAMEID KEY
 * EMIT TO THE ONLY PLAYER LEFT IN THE GAMEROOM THAT VICTORY
 ***********************************************************/
socket.on('disconnect', function() {

	// IF SOMEONE RAGEQUIT, PERFORM GAME CLEANUP AND VICTORY
	if(gameRooms[socket.room]) {
		var winner = getGameRoomOpponent(gameRooms, socket.username);
		userSockets[winner].leave(socket.room);
		socket.leave(socket.room);
		userSockets[winner].emit('victory', {'player': winner});

		updatePlayerStats(winner, socket.username);
		delete gameRooms[socket.room];
		console.log('game rooms after disconnect game end: ' + JSON.stringify(gameRooms));

	}
});


/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* GET MOVESLIST FROM BOTH USERS, CARRY OUT ABILITIES FOR USER GOING FIRST
**************************************************************************************/
socket.on('end turn', function(data) {

	socket.moves = data.moves;
	socket.ready = 'true';
	socket.first = data.first;
	socket.second = data.second;
	socket.third = data.third;
	console.log(data.third.name);
	socket.forth = data.forth;
	socket.fifth = data.fifth;
		
	var turnArray = gameRooms[socket.room].turnArray;
	var opponent = userSockets[getGameRoomOpponent(gameRooms, socket.username)];

	if(bothReady(turnArray)) {

		var turnPlayer = userSockets[turnArray[0]];
		var waitPlayer = userSockets[turnArray[1]];

		var first = turnPlayer.first.name;
		var second = turnPlayer.second.name;
		var third = turnPlayer.third.name;
		var forth = turnPlayer.forth.name;
		var fifth = turnPlayer.fifth.name;
		
		socket.ready = 'false';
		opponent.ready = 'false';

		// PERFORM ALL ABILITIES FROM USER AT ARRAY[0] TO USER AT ARRAY[1]
		performAbilities(turnPlayer, waitPlayer, turnPlayer.moves[third]);
		performAbilities(turnPlayer, waitPlayer, turnPlayer.moves[fifth]);
		performAbilities(turnPlayer, waitPlayer, turnPlayer.moves[second]);
		performAbilities(turnPlayer, waitPlayer, turnPlayer.moves[forth]);
		performAbilities(turnPlayer, waitPlayer, turnPlayer.moves[first]);

		/* PERFORM ALL DEBUFFS FOR WAIT PLAYER IF ANY
		performDebuffs(waitPlayer);
		*/

		// RESET DMG SCALING FOR ALL HEROES
		waitPlayer.first.dmgScaling = 1;
		waitPlayer.second.dmgScaling = 1;
		waitPlayer.third.dmgScaling = 1;
		waitPlayer.forth.dmgScaling = 1;
		waitPlayer.fifth.dmgScaling = 1;

		io.sockets.in(socket.room).emit('first turn', {'turn_player': turnArray[0], 'first': turnPlayer.first, 'second': turnPlayer.second,
														'third':turnPlayer.third, 'forth': turnPlayer.forth, 'fifth': turnPlayer.fifth,
														'wait_first': waitPlayer.first, 'wait_second': waitPlayer.second, 'wait_third':waitPlayer.third, 
														'wait_forth': waitPlayer.forth, 'wait_fifth': waitPlayer.fifth, 'moves':turnPlayer.moves});

	}

});


/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////

/* AFTER CARRYING OUT ABILITIES FOR FIRST USER, CARRY OUT ABILITIES FOR SECOND USER
**************************************************************************************/
socket.on('end turn cont', function(data) {

 	var turnArray = gameRooms[socket.room].turnArray;

 	if(socket.username === turnArray[1]) {

 		var turnPlayer = userSockets[turnArray[1]];
 		var waitPlayer = userSockets[turnArray[0]];

 		var first = turnPlayer.first.name;
		var second = turnPlayer.second.name;
		var third = turnPlayer.third.name;
		var forth = turnPlayer.forth.name;
		var fifth = turnPlayer.fifth.name;

 		// PERFORM ALL ABILITIES FROM USER AT ARRAY[1] TO USER AT ARRAY[0]
		performAbilities(turnPlayer, waitPlayer, turnPlayer.moves[third]);
		performAbilities(turnPlayer, waitPlayer, turnPlayer.moves[fifth]);
		performAbilities(turnPlayer, waitPlayer, turnPlayer.moves[second]);
		performAbilities(turnPlayer, waitPlayer, turnPlayer.moves[forth]);
		performAbilities(turnPlayer, waitPlayer, turnPlayer.moves[first]);

		/* PERFORM ALL DEBUFFS FOR WAIT PLAYER IF ANY
		performDebuffs(waitPlayer);
		*/

		// RESET DMG SCALING FOR ALL HEROES
		waitPlayer.first.dmgScaling = 1;
		waitPlayer.second.dmgScaling = 1;
		waitPlayer.third.dmgScaling = 1;
		waitPlayer.forth.dmgScaling = 1;
		waitPlayer.fifth.dmgScaling = 1;

 		io.sockets.in(socket.room).emit('second turn', {'turn_player': turnArray[1], 'first': turnPlayer.first, 'second': turnPlayer.second,
															'third':turnPlayer.third, 'forth': turnPlayer.forth, 'fifth': turnPlayer.fifth,
															'wait_first': waitPlayer.first, 'wait_second': waitPlayer.second, 'wait_third':waitPlayer.third, 
															'wait_forth': waitPlayer.forth, 'wait_fifth': waitPlayer.fifth, 'moves':turnPlayer.moves});
 	}
});

/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* At the end of both player's moves, synchronize hero card data to client side.
 */

socket.on('end turn update', function() {

	var opponent = userSockets[getGameRoomOpponent(gameRooms, socket.username)];

	socket.emit('resume match', {'enemy_first': opponent.first,
								'enemy_second': opponent.second,
								'enemy_third': opponent.third,
								'enemy_forth': opponent.forth,
								'enemy_fifth': opponent.fifth,
								'first': socket.first,
								'second': socket.second,
								'third': socket.third,
								'forth': socket.forth,
								'fifth': socket.fifth});
});


/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////



}); // END OF SOCKET ON CONNECT




module.exports = app;

console.log('Server listening on port ' + port);



/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


Array.prototype.contains = function(element){
    return this.indexOf(element) > -1;
};

/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* Synchronize socket's Hero Cards with client's provided hero cards provided in parameter data.
 */

function updateHeroes(socket, data) {
	socket.first = data.first;
	socket.second = data.second;
	socket.third = data.third;
	socket.forth = data.forth;
	socket.fifth = data.fifth;
}





/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* Return the opponent socket of the socket given the username.
 */

function getGameRoomOpponent(obj,username) {
	if(userSockets[username]) {
		for(var j = 0; j < obj[userSockets[username].room].length; j++) {
			if(obj[userSockets[username].room][j] != username) {
				return obj[userSockets[username].room][j]
			}
		}
	}
}


/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* Return true iff both sockets in the given array have attribute ready set to true
 */

function bothReady(array) {

	for(var b = 0; b < array.length; b++) {
		if(userSockets[array[b]].ready === 'false') {
			return false;
		}
	}
	return true;
}


/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* Update player profile stats at the end of every game, including disconnects
 */

function updatePlayerStats(winner, loser) {

	// UPDATE WINNERS STUFF
	userModel.findOne({'local.username': winner}, function(err, user) {
		if (err) return err;

        if(user) {
        	user.local.UNRwins += 1;
        	user.local.UNRtotalgames += 1;
        	user.save(function(err) {
            	if (err) throw err;
            	});
        }
        else {
        	console.log("ERROR FINDING WINNER IN SOCKET ON DISCONNECT!");
        }
	});

	// UPDATE LOSERS STUFF
	userModel.findOne({'local.username': loser}, function(err, user) {
		if (err) return err;

        if(user) {
        	user.local.UNRlosses += 1;
        	user.local.UNRtotalgames += 1;
        	user.save(function(err) {
            	if (err) throw err;
            	});
        }
        else {
        	console.log("ERROR FINDING LOSER IN SOCKET ON DISCONNECT!");
        }
	});

}


/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* Set the hero attributes given the socket Hero Card. This should be called
 * only once when the match has first begun.
 */

function initHero(hero) {

	var heroObj = {};

	if(hero === 'Alycia') {
		heroObj.name = 'Alycia';
		heroObj.maxhealth = 500;
		heroObj.health = 500;
		heroObj.maxmana = 150;
		heroObj.mana = 150;
		heroObj.experience = 0;
		heroObj.maxexperience = 125;
		heroObj.level = 1;
		heroObj.debuffs = {};
		heroObj.buffs = {};
		heroObj.respawn = 0;
		heroObj.dmgScaling = 1;
		heroObj.items = {};
	}

	else if (hero === 'Elle') {
		heroObj.name = 'Elle';
		heroObj.maxhealth = 375;
		heroObj.health = 375;
		heroObj.maxmana = 180;
		heroObj.mana = 180;
		heroObj.experience = 0;
		heroObj.maxexperience = 125;
		heroObj.level = 1;
		heroObj.debuffs = {};
		heroObj.buffs = {};
		heroObj.respawn = 0;
		heroObj.dmgScaling = 1;
		heroObj.items = {};
	}

	else if (hero === 'Draconis') {
		heroObj.name = 'Draconis';
		heroObj.health = 450;
		heroObj.maxhealth = 450;
		heroObj.mana = 160;
		heroObj.maxmana = 160;
		heroObj.experience = 0;
		heroObj.maxexperience = 125;
		heroObj.level = 1;
		heroObj.debuffs = {};
		heroObj.buffs = {};
		heroObj.respawn = 0;
		heroObj.dmgScaling = 1;
		heroObj.items = {};
	}

	else if (hero === 'Fontaine') {
		heroObj.name = 'Fontaine';
		heroObj.health = 425;
		heroObj.maxhealth = 425;
		heroObj.mana = 180;
		heroObj.maxmana = 180;
		heroObj.experience = 0;
		heroObj.maxexperience = 125;
		heroObj.level = 1;
		heroObj.debuffs = {};
		heroObj.buffs = {};
		heroObj.respawn = 0;
		heroObj.dmgScaling = 1;
		heroObj.items = {};
	}

	else if (hero === 'Spectra') {
		heroObj.name = 'Spectra';
		heroObj.health = 380;
		heroObj.maxhealth = 380;
		heroObj.mana = 225;
		heroObj.maxmana = 225;
		heroObj.experience = 0;
		heroObj.maxexperience = 125;
		heroObj.level = 1;
		heroObj.debuffs = {};
		heroObj.buffs = {};
		heroObj.respawn = 0;
		heroObj.dmgScaling = 1;
		heroObj.items = {};
	}

	return heroObj;

}


/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* Return the socket that has the hero card with the given name.
 */

function getHero(name, socket) {
	if(name === socket.first.name) {
		return socket.first;
	}
	else if(name === socket.second.name) {
		return socket.second;
	}
	else if(name === socket.third.name) {
		return socket.third;
	}
	else if(name === socket.forth.name) {
		return socket.forth;
	}
	else if(name === socket.fifth.name) {
		return socket.fifth;
	}
}


/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* Check if the hero's health is below 0, if so set the respawn timer
 * of turns. This should be called after application of every ability
 * and every debuff.
 */

function checkDeath(hero) {

	if(hero.health < 0) {
		hero.respawn = 3 + Math.ceil(hero.level/2);
		hero.health = 0;
	}

}



/////
////////
///////////////
///////////////////////
///////////////////////////////
////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
///////////////////////////////
//////////////////////
/////////////
/////////
/////


/* Iterate through player socket's hero cards and apply effects of any debuffs.
 * Need to check all possible debuffs.
 */

function performDebuffs(player) {

















}

////
/////////
////////////////
///////////////////////////////
//////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////
///////////////////////////////
///////////////////
///////////
/////


/* Apply the effects of the ability given the opponent socket and the ability.
 * Need to check all possible abilities.
 */


function performAbilities(sock, opponent, moves) {

	if(moves.targetCard) {
		var targetHero = getHero(moves.targetCard, opponent);
	}

	if(moves.name === 'Blade Strike') {
		moves.amount = Math.ceil(35 * targetHero.dmgScaling);
		targetHero.health -= moves.amount;

		checkDeath(targetHero);
		targetHero.dmgScaling -= 0.15;
	}

	else if(moves.name === 'Iron Arrow') {
		moves.amount = Math.ceil(45 * targetHero.dmgScaling);
		targetHero.health -= moves.amount;

		checkDeath(targetHero);
		targetHero.dmgScaling -= 0.15;
	}

	else if(moves.name === 'Cleave') {
		moves.amount = 40;
		opponent.first.health -= moves.amount
		opponent.second.health -= moves.amount
		opponent.third.health -= moves.amount
		opponent.forth.health -= moves.amount
		opponent.fifth.health -= moves.amount

		checkDeath(opponent.first);
		checkDeath(opponent.second);
		checkDeath(opponent.third);
		checkDeath(opponent.forth);
		checkDeath(opponent.fifth);

		opponent.first.dmgScaling -= 0.15;
		opponent.second.dmgScaling -= 0.15;
		opponent.third.dmgScaling -= 0.15;
		opponent.forth.dmgScaling -= 0.15;
		opponent.fifth.dmgScaling -= 0.15;
	}

	else if(moves.name === 'Lunge') {
		moves.amount = Math.ceil(40 * targetHero.dmgScaling);
		targetHero.health -= moves.amount;

		checkDeath(targetHero);
		targetHero.dmgScaling -= 0.15;
	}

	else if(moves.name === 'Heartache') {
		moves.amount = 30;

		// DURATION OF DEBUFF
		targetHero.debuffs['Heartache'] = 3;
	}

}

//////
///////////////////
////////////////////////////////
//////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


