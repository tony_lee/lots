/******************** MODULES *******************/


var bodyParser      = require('body-parser');
var shortid         = require('shortid');


/******************** MODELS ********************/


var User            = require('../app/models/user');



/******************** ROUTES *********************/



module.exports = function(app, passport) {



        /* MAIN PAGE ROUTE */
        app.get('/', function(req, res) {
            res.render('home.jade', {
                message: req.flash('loginMessage')
            });
        });



        /* USER HOMEPAGE */
        app.get('/dashboard', isLoggedIn, function(req, res) {
            res.render('user.jade', {'total': req.user.local.totalgames, 'wins': req.user.local.wins, 'losses': req.user.local.losses,
                                        'sound': req.user.local.soundInstruct, 'first': req.user.local.handfirst, 'second': req.user.local.handsecond,
                                        'third': req.user.local.handthird, 'forth': req.user.local.handforth, 'fifth': req.user.local.handfifth,
                                        'username': req.user.local.username, 'UNRtotal': req.user.local.UNRtotalgames, 'UNRwins': req.user.local.UNRwins,
                                        'UNRlosses': req.user.local.UNRlosses});
        });



        /* LOG IN ROUTE */
        app.get('/login', passport.authenticate('local-login', {
            successRedirect: '/dashboard',
            failureRedirect: '/',
            failureFlash: true
        }));



        /* LOG OUT ROUTE */
        app.get('/logout', function(req, res) {
            req.logout();
            res.redirect('/');
        });




        /* SIGN UP ROUTE */
        app.post('/signup', passport.authenticate('local-signup', {
            successRedirect: '/dashboard',
            failureRedirect: '/',
            failureFlash: true
        }));



/*************************************************** SET HERO CARD **********************************************/


        /* SET FIRST HERO CARD ROUTE */
        app.post('/users/:user/firstset', isLoggedIn, function(req, res) {

            User.findOne({ 'local.username': req.params.user}, function(err, user) {
                if (err) return err;

                if(user) {

                    if(req.user.local.username === user.local.username) {

                        user.local.handfirst = req.body.heroCard;
                        user.save(function(err) {
                            if (err) throw err;
                        });
                    }

                    res.json(req.body.heroCard);

                }

                else {
                    res.status(404).send('Error finding user updating first card.');
                }
            });

        });


        /* SET SECOND HERO CARD ROUTE */
        app.post('/users/:user/secondset', isLoggedIn, function(req, res) {

            User.findOne({ 'local.username': req.params.user}, function(err, user) {
                if (err) return err;

                if(user) {

                    if(req.user.local.username === user.local.username) {
                        user.local.handsecond = req.body.heroCard;
                        user.save(function(err) {
                            if (err) throw err;
                        });
                    }

                    res.json(req.body.heroCard);

                }

                else {
                    res.status(404).send('Error finding user updating second card.');
                }
            });

        });


        /* SET THIRD HERO CARD ROUTE */
        app.post('/users/:user/thirdset', isLoggedIn, function(req, res) {

            User.findOne({ 'local.username': req.params.user}, function(err, user) {
                if (err) return err;

                if(user) {

                    if(req.user.local.username === user.local.username) {
                        user.local.handthird = req.body.heroCard;
                        user.save(function(err) {
                            if (err) throw err;
                        });
                    }

                    res.json(req.body.heroCard);

                }

                else {
                    res.status(404).send('Error finding user updating third card.');
                }
            });

        });



        /* SET FORTH HERO CARD ROUTE */
        app.post('/users/:user/forthset', isLoggedIn, function(req, res) {

            User.findOne({ 'local.username': req.params.user}, function(err, user) {
                if (err) return err;

                if(user) {

                    if(req.user.local.username === user.local.username) {
                        user.local.handforth = req.body.heroCard;
                        user.save(function(err) {
                            if (err) throw err;
                        });
                    }

                    res.json(req.body.heroCard);

                }

                else {
                    res.status(404).send('Error finding user updating forth card.');
                }
            });

        });


        /* SET FIRST HERO CARD ROUTE */
        app.post('/users/:user/fifthset', isLoggedIn, function(req, res) {

            User.findOne({ 'local.username': req.params.user}, function(err, user) {
                if (err) return err;

                if(user) {

                    if(req.user.local.username === user.local.username) {
                        user.local.handfifth = req.body.heroCard;
                        user.save(function(err) {
                            if (err) throw err;
                        });
                    }

                    res.json(req.body.heroCard);

                }

                else {
                    res.status(404).send('Error finding user updating fifth card.');
                }
            });

        });


        /* GET USER CARDS */
        app.get('/users/:user/cards', isLoggedIn, function(req, res) {

            User.findOne({ 'local.username': req.params.user}, function(err, user) {
                if (err) return err;

                if(user) {

                    if(req.user.local.username === user.local.username) {
                        res.json({'first': user.local.handfirst, 'second': user.local.handsecond,
                                'third': user.local.handthird, 'forth': user.local.handforth, 'fifth': user.local.handfifth});
                    }

                }

                else {
                    res.status(404).send('Error finding user getting cards.');
                }
            });

        });


/*********************************************** END OF SET HERO CARD ROUTES **************************************/

    
        // NO MORE SOUND TUTORIAL DISPLAY ROUTE
        app.post('/users/:user/sound', isLoggedIn, function(req, res) {

            User.findOne({ 'local.username': req.params.user}, function(err, user) {
                if (err) return err;

                if(user) {

                    if(req.user.local.username === user.local.username) {
                        user.local.soundInstruct = 'yes';
                        user.save(function(err) {
                            if (err) throw err;
                        });
                    }

                    res.json('success');

                }

                else {
                    res.status(404).send('Error finding user updating sound tutorial.');
                }
            });

        });


/*************************************** MATCHMAKING ROUTES *****************************/

    // UNRANKED ROUTE
    app.get('/game_unranked', isLoggedIn, function(req, res) {

        res.render('game.jade', {'total': req.user.local.totalgames, 'wins': req.user.local.wins, 'losses': req.user.local.losses,
                                        'sound': req.user.local.soundInstruct, 'first': req.user.local.handfirst, 'second': req.user.local.handsecond,
                                        'third': req.user.local.handthird, 'forth': req.user.local.handforth, 'fifth': req.user.local.handfifth,
                                        'username': req.user.local.username, 'UNRtotal': req.user.local.UNRtotalgames, 'UNRwins': req.user.local.UNRwins,
                                        'UNRlosses': req.user.local.UNRlosses, match: 'Unranked'});

    });

    // RANKED ROUTE
    app.get('/game_ranked', isLoggedIn, function(req, res) {

        res.render('game.jade', {'total': req.user.local.totalgames, 'wins': req.user.local.wins, 'losses': req.user.local.losses,
                                        'sound': req.user.local.soundInstruct, 'first': req.user.local.handfirst, 'second': req.user.local.handsecond,
                                        'third': req.user.local.handthird, 'forth': req.user.local.handforth, 'fifth': req.user.local.handfifth,
                                        'username': req.user.local.username, 'UNRtotal': req.user.local.UNRtotalgames, 'UNRwins': req.user.local.UNRwins,
                                        'UNRlosses': req.user.local.UNRlosses, match: 'Ranked'});

    });




    /* 404 ROUTE, SHOULD BE FARTHEST BELOW */
    app.get('/*', function(req, res) {
         res.status(404).send('No such page.');
     });






        /* VERIFICATION MIDDLEWARE USING PASSPORT */
        function isLoggedIn(req, res, next) {

            if (req.isAuthenticated())
                return next();
            res.redirect('/');
        }






 } // EXPORTS