
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');//https://www.npmjs.com/package/bcrypt-nodejs

//defining user schema
var userSchema = mongoose.Schema({

    local    : {
		username	 		: {type: String, unique : true, required : true},
		password     		: { type: String, required : true},
		description  		: {type: String, default: "New player"},
		handfirst			: {type: Object, default: "empty"},
		handsecond			: {type: Object, default: "empty"},
		handthird			: {type: Object, default: "empty"},
		handforth			: {type: Object, default: "empty"},
		handfifth			: {type: Object, default: "empty"},
		totalgames			: {type: Number, default: 0},
		wins				: {type: Number, default: 0},
		losses				: {type: Number, default: 0},
		soundInstruct		: {type: String, default: "no"},
		UNRtotalgames			: {type: Number, default: 0},
		UNRwins					: {type: Number, default: 0},
		UNRlosses				: {type: Number, default: 0},
		perm   		        : { type: String, required: true}

	}
   
});

//hashing passwords for secure storage
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

//checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', userSchema);
