/* RESPONSIVE SIDE BAR
 * CHECK WINDOW SIZE ON INIT
 * IF GREATER THAN, SHOW THE SIDEBAR
 * IF LESS THAN, HIDE THE SIDEBAR
 ****************************************************/

$(window).resize(function() {
	var path = $(this);
	var contW = path.width();
	if(contW >= 1775){
		document.getElementsByClassName("sidebar-toggle")[0].style.left="200px";
	}else{
		document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
	}
});


/* INITIALIZE VARIABLES
 * TRACKING VARIABLES IF USER WANTS TO SELECT HERO IN HAND
 * SOUND VARIABLES
 ********************************************************/

var firstTrack 		= false;
var secondTrack 	= false;
var thirdTrack 		= false;
var forthTrack 		= false;
var fifthTrack 		= false;

// GENERAL SOUNDS
var swish 	= new Audio('sounds/swish.mp3');
var select 	= new Audio('sounds/select.mp3');
var error 	= new Audio('sounds/error.mp3');
var market	= new Audio('sounds/market.mp3');


// HERO DIALOGUE SOUNDS
var alyciaSelect 	= new Audio('sounds/dialogue/alycia.mp3');
var elleSelect		= new Audio('sounds/dialogue/elle.mp3');
var draconisSelect 	= new Audio('sounds/dialogue/draconis.mp3');
var reynaldSelect 	= new Audio('sounds/dialogue/reynald.mp3');
var spectraSelect 	= new Audio('sounds/dialogue/spectra.mp3');




/* USER.JADE
 * LOCAL STORAGE SETTING SOUND
 * SET LOCAL VARIABLES FROM USER DATA PASSED FROM ROUTE
 * LOAD HERO DESCRIPTIONS FROM THEIR SEPERATE HTML
 * CALCULATE AND DISPLAY WIN RATIO ON PROFILE TAB
 * RANDOMIZE AND SELECT MUSIC CONTROL
 * VOLUME CONTROL ON SIDEBAR
 * SIDEBAR CONTROLS
 * RESET TRACKING ON CLICKING ANY TAB OTHER THAN CARDS
 * SETTING HERO CARDS INTO HAND
 * CHECK IF CAN INTO MATCHMAKING
 ***********************************************************************/


$(document).ready(function() {


	// REFRESH PAGE WHEN USER GOES BACK 
	var $input = $('#refresh');

    $input.val() == 'yes' ? location.reload(true) : $input.val('yes');

	/********* LOCAL STORAGE AND USER DATA ************/


	var local_volume		= localStorage.getItem("vol");


	// SET VOLUME BAR IF SOUND STORED IN LOCAL
	if(local_volume) {
		$('.volumeBar').css('width', local_volume*100 + '%');

		if(local_volume == 0) {
			$('#muteIcon').show();
		}

		setVolume(local_volume);
	}

	var local_username = $('#username').text();
	var local_wins = document.getElementById('handCards').dataset.wins;
	var local_losses = document.getElementById('handCards').dataset.losses;
	var local_totalgames = document.getElementById('handCards').dataset.total;
	var local_ratio = (local_wins/local_totalgames)*100;


	/****** LOAD HEROES DESCRIPTS *****/


	$('#kayleaDescript').load('heroes/alycia.html');
	$('#asheaDescript').load('heroes/elle.html');
	$('#jarvanaDescript').load('heroes/draconis.html');
	$('#garenaDescript').load('heroes/fontaine.html');
	$('#leblancaDescript').load('heroes/spectra.html');



	/******** WIN RATIO FOR USER PROFILE **********/


	if(local_totalgames > 1) {
		$('#ratio').text(local_ratio+'%');
	}
	else {
		$('#ratioDescript').text('No ranked games played');
	}

	if(local_totalgames > 20) {

		//ADAMANTITE
		if(local_ratio >= 90 ) {
			$('#ratio').css({'color': '#1a3300'});
		}

		//MYTHRIL
		if(local_ratio >= 80 && local_ratio < 90) {
			$('#ratio').css({'color': '#1a3366'});
		}

		//GOLD
		if(local_ratio >= 70 && local_ratio < 80) {
			$('#ratio').css({'color': '#FFD700'});
		}

		//SILVER
		if(local_ratio >= 50 && local_ratio < 70) {
			$('#ratio').css({'color': '#cccccc'});
		}

		//BRONZE
		if(local_ratio < 50) {
			$('#ratio').css({'color': '#CD7F32'});
		}

	}


	/************* MUSIC/SOUND CONTROLS **************/


	/*** RANDOMIZE AND LOOP BACKGROUND MUSIC ***/


	var collection=[];
	var loadedIndex=0;

	// FUNCTION INIT TAKES IN ARRAY OF PATHS TO SOUND FILES
	function init(audios) {

	  for(var i = 0; i < audios.length; i++) {

	    var audio = new Audio(audios[i]);

	    if(local_volume) {
	  		audio.volume = local_volume;
	  	}

	    collection.push(audio);
	    buffer(audio);
	  
	  }
	}

	function buffer(audio) {
	  if(audio.readyState==4)return loaded();
	  setTimeout(function(){buffer(audio)},100);
	}

	function loaded() {
	  loadedIndex++;
	  if(collection.length==loadedIndex)playLooped();
	}

	function playLooped() {
	  var audio=Math.floor(Math.random() * (collection.length));
	  audio=collection[audio];
	  audio.play();
	  setTimeout(playLooped,(audio.duration*1000)+1000);
	}

	/***** SET PATH TO SOUND FILES HERE ******/
	init([
	  'sounds/celtic.mp3',
	  'sounds/wolf.mp3',
	  'sounds/slow.mp3'
	]);


	/********* VOLUME BAR AND GENERAL SOUND ********/


	// IF USERS FIRST TIME LOGGING IN, SHOW WELCOME SIGN (CURRENTLY ABOUT SOUND)
	if(document.getElementById('handCards').dataset.sound == 'no') {
		$('#soundModal').modal('show');		// SOUND MODAL IS JUST A GENERAL WELCOME SIGN
	}

	// SEND THAT THE USER HAS LOGGED IN FOR THE FIRST TIME, DON'T SHOW MODAL SUBSEQUENT LOGINS
	$('#soundConfirm').on('click', function() {
		$.ajax({
          type: 'POST',
          url: '/users/'+local_username+'/sound',
          success: function() {
          }
      	});
	});



	/******** VOLUME BAR ******/

	var volumeDrag = false;
	$('.volume').on('mousedown', function (e) {
	    volumeDrag = true;
	    $('.sound').removeClass('muted');
	    updateVolume(e.pageX);
	});
	$(document).on('mouseup', function (e) {
	    if (volumeDrag) {
	        volumeDrag = false;
	        updateVolume(e.pageX);
	    }
	});
	$(document).on('mousemove', function (e) {
	    if (volumeDrag) {
	        updateVolume(e.pageX);
	    }
	});
	var updateVolume = function (x, vol) {
	    var volume = $('.volume');
	    var percentage;
	    //if only volume have specificed
	    //then direct update volume
	    if (vol) {
	        percentage = vol * 100;
	    } else {
	        var position = x - volume.offset().left;
	        percentage = 100 * position / volume.width();
	    }

	    if (percentage > 100) {
	        percentage = 100;
	    }
	    if (percentage < 0) {
	        percentage = 0;
	    }

	    // SHOW OR HIDE MUTE SIGN BASED ON VOLUME
	    if(percentage/100 > 0) {
	    	$('#muteIcon').hide();
	    }
	    else {
	    	$('#muteIcon').show();
	    }

	    /* UPDATE VOLUME FOR:
	     * DISPLAYING CSS BAR
	     * ALL THE RANDOMIZED SONGS IN COLLECTIONS ARRAY
	     * SET LOCAL STORAGE SOUND
	     * ALL OTHER SOUNDS
	     ***********************************************/

	    $('.volumeBar').css('width', percentage + '%');

	    for(var j = 0; j < collection.length; j++) {
	    	collection[j].volume = percentage / 100;
	    }

	    localStorage.setItem("vol", percentage/100);

	    market.volume = percentage / 100;
	    select.volume = percentage / 100;
	    swish.volume = percentage / 100;
	    error.volume = percentage / 100;
	    alyciaSelect.volume = percentage / 100;
	    elleSelect.volume = percentage / 100;
	    draconisSelect.volume = percentage / 100;
	    reynaldSelect.volume = percentage / 100;
	    spectraSelect.volume = percentage / 100;

	};  // END UPDATEVOLUME


	/*********** SIDE BAR *************/

	$('.dropdown').on('show.bs.dropdown', function(e){
	    $(this).find('.dropdown-menu').first().stop(true, true).slideDown(200);
	});
	$('.dropdown').on('hide.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp(200);
	});
	$("#menu-toggle").click(function(e) {
		e.preventDefault();
		var elem = document.getElementById("sidebar-wrapper");
		left = window.getComputedStyle(elem,null).getPropertyValue("left");
		if(left == "200px"){
			document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
		}
		else if(left == "-200px"){
			document.getElementsByClassName("sidebar-toggle")[0].style.left="200px";
		}
	});


	/********** RESET TRACK VARIABLES ON OTHER TABS CLICK ******/

	$('.inside').on('click', function(e) {
		if(e.target == this || $(e.target).attr('class') == 'head1') {
			sideBar(e);
		}
	});

	$('.heroDescript').on('click', function(e) {
			sideBar(e);
	});


	$('li.mm, li.hand, li.store, li.profile').on('click', function(e) {
		sideBar(e);
		resetTrack();
	});

	$('li.cards').on('click', function(e) {
		sideBar(e);
	});

	$('li.mm').on('click', function() {

		if(!$('li.mm').hasClass('active') && !$('li.hand').hasClass('active') && !$('li.cards').hasClass('active') && !$('li.profile').hasClass('active')) {
			$('#wallpaper').fadeOut(50);
			setTimeout(function() {
			$('#wallpaper').css("background-image", "url(images/landscape.jpg)"); 
			$('#wallpaper').fadeIn(400);
			}, 100);
		}

		if(!$('li.mm').hasClass('active')) {
			swish.currentTime=0;
			swish.play();
		}


	});

	// CLICK ON STORE, CHANGE BACKGROUND AND PLAY SOUND
	$('li.store').on('click', function() {

		if(!$('li.store').hasClass('active')) {

			$('#wallpaper').fadeOut(50);
			setTimeout(function() {
			$('#wallpaper').css("background-image", "url(images/market.jpg)"); 
			$('#wallpaper').fadeIn(400);
			}, 100);
			market.currentTime=0;
			market.play();
		}
	});

	// CLICK ON HAND, CHANGE BACKGROUND
	$('li.hand').on('click', function() {

		if(!$('li.mm').hasClass('active') && !$('li.hand').hasClass('active') && !$('li.cards').hasClass('active') && !$('li.profile').hasClass('active')) {
			$('#wallpaper').fadeOut(50);
			setTimeout(function() {
			$('#wallpaper').css("background-image", "url(images/landscape.jpg)"); 
			$('#wallpaper').fadeIn(400);
			}, 100);
		}
	});

	// CLICK ON CARDS, CHANGE BACKGROUND
	$('li.cards').on('click', function() {

		if(!$('li.mm').hasClass('active') && !$('li.hand').hasClass('active') && !$('li.cards').hasClass('active') && !$('li.profile').hasClass('active')) {
			$('#wallpaper').fadeOut(50);
			setTimeout(function() {
			$('#wallpaper').css("background-image", "url(images/landscape.jpg)"); 
			$('#wallpaper').fadeIn(400);
			}, 100);
		}
	});

	// CLICK ON PROFILE, CHANGE BACKGROUND
	$('li.profile').on('click', function() {

		if(!$('li.mm').hasClass('active') && !$('li.hand').hasClass('active') && !$('li.cards').hasClass('active') && !$('li.profile').hasClass('active')) {
			$('#wallpaper').fadeOut(50);
			setTimeout(function() {
			$('#wallpaper').css("background-image", "url(images/landscape.jpg)"); 
			$('#wallpaper').fadeIn(400);
			}, 100);
		}
	});



	/*************** MATCHMAKING ****************/


	//CHECK IF USER HAS ALL HERO CARDS SET
	$('#ranked').on('click', function(e) {
		e.preventDefault();
		if(!checkHandSet()) {
			$('#errorModal').modal();
		}
		else {
			window.location.href = '/game_ranked';
		}
	});

	$('#unranked').on('click', function(e) {
		e.preventDefault();
		if(!checkHandSet()) {
			$('#errorModal').modal();
		}
		else {
			window.location.href = '/game_unranked';
		}
	});

	//PRESSING CONFIRM ON ERROR MODAL, GUIDE USER TO HAND TAB
	$('#confirm').on('click', function() {
		$('.mm').next('li').find('a').trigger('click');
	});

	

	/************** SET HERO CARD ****************/


	//SET FIRST CARD
	$('#firstCard').on('click', function(event) {
		firstTrack = true;
		$('.hand').next('li').find('a').trigger('click');
		window.scrollTo(0, 0);
	});

	//SET SECOND CARD
	$('#secondCard').on('click', function() {
		secondTrack = true;
		$('.hand').next('li').find('a').trigger('click');
		window.scrollTo(0, 0);
	});

	//SET THIRD CARD
	$('#thirdCard').on('click', function() {
		thirdTrack = true;
		$('.hand').next('li').find('a').trigger('click');
		window.scrollTo(0, 0);
	});

	//SET FORTH CARD
	$('#forthCard').on('click', function() {
		forthTrack = true;
		$('.hand').next('li').find('a').trigger('click');
		window.scrollTo(0, 0);
	});

	//SET FIFTH CARD
	$('#fifthCard').on('click', function() {
		fifthTrack = true;
		$('.hand').next('li').find('a').trigger('click');
		window.scrollTo(0, 0);
	});



	/************** SELECTING HERO CARDS *****************/

	/* CLICK ON A HERO CARD IN CARDS TAB
	 * IF WE WERE TRACKING TO SET, GO SET THE HERO TO HAND
	 * IF NOT, DISPLAY HERO INFORMATION WITH BADASS HERO DIALOGUE
	 ***********************************************************/
	$('.heroCard').on('click', function(event) {
		if(firstTrack || secondTrack || thirdTrack || forthTrack || fifthTrack) {
			clickHero(event);
		}

		// REDIRECT TO HERO INFORMATION PAGE
		else {
			if(event.target.id == 'Alycia') {
				playDialogue(alyciaSelect);
				$('.kaylea').find('a').click();
				$('.kaylea').removeClass("active");
				window.scrollTo(0, 0);
			}
			else if(event.target.id =='Elle') {
				playDialogue(elleSelect);
				$('.ashea').find('a').click();
				$('.ashea').removeClass("active");
				window.scrollTo(0, 0);
			}
			else if(event.target.id =='Draconis') {
				playDialogue(draconisSelect);
				$('.jarvana').find('a').click();
				$('.jarvana').removeClass("active");
				window.scrollTo(0, 0);
			}
			else if(event.target.id =='Fontaine') {
				playDialogue(reynaldSelect);
				$('.garena').find('a').click();
				$('.garena').removeClass("active");
				window.scrollTo(0, 0);
			}
			else if(event.target.id =='Spectra') {
				playDialogue(spectraSelect);
				$('.leblanca').find('a').click();
				$('.leblanca').removeClass("active");
				window.scrollTo(0, 0);
			}
		}
	});




/******* PREVENT DIALOG OVERLAP ******/

function playDialogue(sound) {
	alyciaSelect.pause();
	elleSelect.pause();
	reynaldSelect.pause();
	draconisSelect.pause();
	spectraSelect.pause();
	reynaldSelect.currentTime=0;
	draconisSelect.currentTime=0;
	elleSelect.currentTime=0;
	alyciaSelect.currentTime=0;
	spectraSelect.currentTime=0;
	sound.play();
}



}); //END DOCUMENT READY



// CLOSING RESPONSIVE SIDEBAR ON CLICKING LI ELEMENT
function sideBar(e) {
	e.preventDefault();
	var elem = document.getElementById("sidebar-wrapper");
	left = window.getComputedStyle(elem,null).getPropertyValue("left");
	if(left == "200px" && $(window).width() < 1780){
		document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
	}
}

//CHECK IF A USER HAS NO EMPTY SLOT CARDS
function checkHandSet() {
	if(document.getElementById('handCards').dataset.first == 'empty' ||
		document.getElementById('handCards').dataset.second == 'empty' ||
		document.getElementById('handCards').dataset.third == 'empty' ||
		document.getElementById('handCards').dataset.forth == 'empty' ||
		document.getElementById('handCards').dataset.fifth == 'empty')
	{
		return false;
	}
	else {
		return true;
	}
}


/* SETTING HERO CARDS
 * EACH POSITION HERO CARD HAS THEIR UNIQUE ROUTE
 * CALL AJAX
 */
function clickHero(e) {


	var local_username = $('#username').text();

	if(firstTrack && noDuplicatesFirst.apply(this, arguments)) {
		firstTrack = false;

		$.ajax({
          type: 'POST',
          url: '/users/'+local_username+'/firstset',
          data: { heroCard: e.target.id},
          success: function(data) {
          	select.currentTime=0;
			select.play();
          	$('#firstCard').attr("src", '/images/heroes/'+data+'.jpg');
          	document.getElementById('handCards').dataset.first = data;
            $('.mm').next('li').find('a').trigger('click');
            window.scrollTo(0, 0);
            $('#equipModal').modal('show');

          }
      	});

	}


	else if (secondTrack && noDuplicatesSecond.apply(this, arguments)) {
		secondTrack = false;

		$.ajax({
          type: 'POST',
          url: '/users/'+local_username+'/secondset',
          data: { heroCard: e.target.id},
          success: function(data) {
          	select.currentTime=0;
			select.play();
          	$('#secondCard').attr("src", '/images/heroes/'+data+'.jpg');
          	document.getElementById('handCards').dataset.second = data;
            $('.mm').next('li').find('a').trigger('click');
            window.scrollTo(0, 0);
            $('#equipModal').modal('show');
            
          }
      	});

	}


	else if (thirdTrack && noDuplicatesThird.apply(this, arguments)) {
		thirdTrack = false;

		$.ajax({
          type: 'POST',
          url: '/users/'+local_username+'/thirdset',
          data: { heroCard: e.target.id},
          success: function(data) {
          	select.currentTime=0;
			select.play();
          	$('#thirdCard').attr("src", '/images/heroes/'+data+'.jpg');
          	document.getElementById('handCards').dataset.third = data;
            $('.mm').next('li').find('a').trigger('click');
            window.scrollTo(0, 0);
            $('#equipModal').modal('show');

          }
      	});

	}


	else if (forthTrack && noDuplicatesForth.apply(this, arguments)) {
		forthTrack = false;

		$.ajax({
          type: 'POST',
          url: '/users/'+local_username+'/forthset',
          data: { heroCard: e.target.id},
          success: function(data) {
          	select.currentTime=0;
			select.play();
          	$('#forthCard').attr("src", '/images/heroes/'+data+'.jpg');
          	document.getElementById('handCards').dataset.forth = data;
            $('.mm').next('li').find('a').trigger('click');
            window.scrollTo(0, 0);
            $('#equipModal').modal('show');

          }
      	});

	}


	else if (fifthTrack && noDuplicatesFifth.apply(this, arguments)) {
		fifthTrack = false;

		$.ajax({
          type: 'POST',
          url: '/users/'+local_username+'/fifthset',
          data: { heroCard: e.target.id},
          success: function(data) {
          	select.currentTime=0;
			select.play();
          	$('#fifthCard').attr("src", '/images/heroes/'+data+'.jpg');
          	document.getElementById('handCards').dataset.fifth = data;
            $('.mm').next('li').find('a').trigger('click');
            window.scrollTo(0, 0);
            $('#equipModal').modal('show');

          }
      	});

	}
} // END CLICKHERO()




/************* FINDING DUPLICATES NOT SELF HERO CARD *******************/
function noDuplicatesFirst(e) {


	if(firstTrack && e.target.id == document.getElementById('handCards').dataset.second || 
		e.target.id == document.getElementById('handCards').dataset.third ||
		e.target.id == document.getElementById('handCards').dataset.forth || 
		e.target.id == document.getElementById('handCards').dataset.fifth) {
		error.currentTime=0;
		error.play();
		$('#dupsModal').modal('show');
		return false;
	}
	else {
		return true;
	}

}

function noDuplicatesSecond(e) {

	if(secondTrack && e.target.id == document.getElementById('handCards').dataset.first || 
		e.target.id == document.getElementById('handCards').dataset.third ||
		e.target.id == document.getElementById('handCards').dataset.forth || 
		e.target.id == document.getElementById('handCards').dataset.fifth) {
		error.currentTime=0;
		error.play();
		$('#dupsModal').modal('show');
		return false;
	}
	else {
		return true;
	}

}

function noDuplicatesThird(e) {

	if(thirdTrack && e.target.id == document.getElementById('handCards').dataset.second || 
		e.target.id == document.getElementById('handCards').dataset.first ||
		e.target.id == document.getElementById('handCards').dataset.forth || 
		e.target.id == document.getElementById('handCards').dataset.fifth) {
		error.currentTime=0;
		error.play();
		$('#dupsModal').modal('show');
		return false;
	}
	else {
		return true;
	}

}

function noDuplicatesForth(e) {

	if(forthTrack && e.target.id == document.getElementById('handCards').dataset.second || 
		e.target.id == document.getElementById('handCards').dataset.third ||
		e.target.id == document.getElementById('handCards').dataset.first || 
		e.target.id == document.getElementById('handCards').dataset.fifth) {
		error.currentTime=0;
		error.play();
		$('#dupsModal').modal('show');
		return false;
	}
	else {
		return true;
	}

}

function noDuplicatesFifth(e) {

	if(fifthTrack && e.target.id == document.getElementById('handCards').dataset.second || 
		e.target.id == document.getElementById('handCards').dataset.third ||
		e.target.id == document.getElementById('handCards').dataset.forth || 
		e.target.id == document.getElementById('handCards').dataset.first) {
		error.currentTime=0;
		error.play();
		$('#dupsModal').modal('show');
		return false;
	}
	else {
		return true;
	}

}

function resetTrack() {
	firstTrack = false;
	secondTrack = false;
	thirdTrack = false;
	forthTrack = false;
	fifthTrack = false;
}

// ADD SOUNDS TO THIS SO SOUND VOLUME CAN BE INITIALIZED FROM LOCAL STORAGE
function setVolume(vol) {
	market.volume = vol;
	select.volume = vol;
    swish.volume = vol;
    error.volume = vol;
    alyciaSelect.volume = vol;
    elleSelect.volume = vol;
    draconisSelect.volume = vol;
    reynaldSelect.volume = vol;
    spectraSelect.volume = vol;
}