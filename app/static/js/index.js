/* RESPONSIVE SIDE BAR
 * CHECK WINDOW SIZE ON INIT
 * IF GREATER THAN, SHOW THE SIDEBAR
 * IF LESS THAN, HIDE THE SIDEBAR
 ****************************************************/

$(window).resize(function() {
	var path = $(this);
	var contW = path.width();
	if(contW >= 1775){
		document.getElementsByClassName("sidebar-toggle")[0].style.left="200px";
	}else{
		document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
	}
});


/* HOME.JADE
 * REMEMBER ME FEATURE (USERNAMES ONLY) USING COOKIES
 * SIDEBAR CONTROL
 * QUICK LINK TO SIGNUP FROM GUIDE PAGE
 * GET AND SET ERROR MESSAGES FROM PASSPORT
 ***************************************************/
$(document).ready(function() {


	/**************** COOKIES ****************/

	checkCookie();

	$('#rememberMe').on('click', function() {
		if($('#rememberMe').is(':checked')) {

			if($('#loginInputUsername').val() != '') {
				document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
				setCookie("username", $('#loginInputUsername').val(), 365);
			}
		}
		else {
			document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
		}
	});

	$('#loginInputUsername').on('change keydown paste input', function() {
		document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";

		if($('#rememberMe').is(':checked')) {
			$(function () {
    		$('#flashlabel').animate({ opacity: 0 }, 200);
    		$('#flashlabel').animate({ opacity: 1 }, 200);
			});
		}
		$("#rememberMe").prop("checked", false);


	});



	/***************** SIDE BAR *****************/

	$('.dropdown').on('show.bs.dropdown', function(e){
	    $(this).find('.dropdown-menu').first().stop(true, true).slideDown(200);
	});
	$('.dropdown').on('hide.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp(200);
	});
	$("#menu-toggle").click(function(e) {
		e.preventDefault();
		var elem = document.getElementById("sidebar-wrapper");
		left = window.getComputedStyle(elem,null).getPropertyValue("left");
		if(left == "200px"){
			document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
		}
		else if(left == "-200px"){
			document.getElementsByClassName("sidebar-toggle")[0].style.left="200px";
		}
	});


	$('li.news, li.login, li.signup, li.guide, li.about').on('click', function(e) {
		e.preventDefault();
		var elem = document.getElementById("sidebar-wrapper");
		left = window.getComputedStyle(elem,null).getPropertyValue("left");
		if(left == "200px" && $(window).width() < 1780){
			document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
		}
	});



	/************** LINK TO SIGNUP FROM GUIDE ****************/

	$('#newbie').on('click', function() {
		$("li.signup").find('a').trigger('click');
  		window.scrollTo(0, 0);
	});

	/***************** HANDLE ERROR LOGIN/SIGNUP ******************/

		// IF LOGIN ERROR, RETURN TO LOGIN PILL AND SHOW ERROR MESSAGE
		if($('#message').text() == "No user with that username exists" || $('#message').text() == "Invalid password") {
			$('li.login').find('a').trigger('click');
		}

		$('li.login').on('click', function() {
			$('#logError').text('');
			if($('#rememberMe').is(':checked')) {
				setTimeout(function () {
				$('#loginInputPassword').focus();
    			}, 400)
			}
			else {
			setTimeout(function () {
				$('#loginInputUsername').focus();
    		}, 400)
		}
		});

		// IF SIGNUP ERROR, RETURN TO SIGNUP PILL AND SHOW ERROR MESSAGE
		if($('#message').text() == "Your passwords do not match" || $('#message').text() == "That username is already taken") {
			$("li.signup").find('a').trigger('click');
		}

		$('li.signup').on('click', function() {
			$('#logError').text('');
			setTimeout(function () {
				$('#InputUsername').focus();
    		}, 400)
		});


}); // END READY



/**** COOKIE HELPER FUNCS FROM W3SCHOOLS ****/

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}	

function checkCookie() {
    var username=getCookie("username");
    if (username!="") {
        $('#loginInputUsername').val(username);
        $("#rememberMe").prop("checked", true);

    }else{
        }
}