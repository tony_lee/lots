var LocalStrategy = require('passport-local').Strategy;

var User = require('../app/models/user');

module.exports = function(passport) {

    String.prototype.capitalizeFirstLetter = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }
	
	//serializing and deserializing users for persistent sessions
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
	
	//local sign-up strategy
    passport.use('local-signup', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, username, password, done) {

            username = username.toLowerCase().capitalizeFirstLetter();

			//check if user in database
            User.findOne({
                'local.username': username
            }, function(err, user) {
                if (err)
                    return done(err);
				//use connect-flash to  send message to user
                if (req.body.password != req.body.confpassword) {
                    return done(null, false, req.flash('loginMessage', 'Your passwords do not match'));
                }
                if (user) {
                    return done(null, false, req.flash('loginMessage', 'That username is already taken'));
                } else {

                    var newUser = new User();
                    newUser.local.username = username;
                    newUser.local.password = newUser.generateHash(password); // use the generateHash function in our user model         
		    
		 User.count({}, function(err, entrycount) {
			console.log("IN COUNT: " + entrycount);
                        if (entrycount == 0) {
                            newUser.local.perm = "admin";
				
                        } else {
                            newUser.local.perm = "user";}
			newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });

			});     
		         
                    // save the user

					
                }

            });

        }));

	//login strategy
    passport.use('local-login', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, username, password, done) {

            username = username.toLowerCase().capitalizeFirstLetter();

			//check if user in database
            User.findOne({
                'local.username': username
            }, function(err, user) {
                if (err)
                    return done(err);

                if (!user)
                    return done(null, false, req.flash('loginMessage', 'No user with that username exists')); // req.flash is the way to set flashdata using connect-flash
				
				//calling validPassword in user.js to check if hashes are same
                if (!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Invalid password')); // create the loginMessage and save it to session as flashdata

                return done(null, user);
            });

        }));

};
